/**
 * @Package
 * @auhter 枯木Kum
 * @date 2021/6/4-2:16 PM
 * <p>...</p>
 * @version V1.0
 */

fun main() {
    val people = People()
    people.eat()

}


open class Animal(){
    protected var cName:String = "动物"

    init{
        println("你创建了一个动物")
    }

    constructor(name:String) : this() {
        this.cName = name
    }

    open fun eat(){
        println("${cName}很粗劣的吃着饭")
    }
}

class People : Animal("人类"){
    override fun eat() {
        println("大多数${cName}都会文明的吃饭")
    }
}