import com.sun.org.apache.xml.internal.security.Init

/**
 * @Package
 * @auhter 枯木Kum
 * @date 2021/6/4-2:46 PM
 * <p>...</p>
 * @version V1.0
 */

fun main() {
    val user1 = SysUser("admin", "admin", 18, 1)
    val user2 = SysUser("admin", "admin", 118, 3)
    var (name, pwd, age) = user1
    println("${name},${pwd},${age}")

    println(user1 + user2)

}

data class SysUser(
    val userName: String,
    val passWord: String,
    val age: Int,
    val sex: Int
) {
    /**
     * 操作符重载
     * a + b puls
     * a - b minus
     * a * b times
     * a / b div
     * a % b rem
     * 符合重载是以上方式 + Assign
     */
    operator fun plus(o: SysUser): SysUser {
        return SysUser(userName, passWord, age + o.age, sex + o.sex)
    }
}