/**
 * @Package
 * @auhter 枯木Kum
 * @date 2021/6/4-9:55 AM
 * <p>...</p>
 * @version V1.0
 */

fun main(args: Array<String>) {
    f()
}

fun f() {
    val arr = arrayOf(1, 2, 3, 4)
    arr.forEach { i: Int ->
        println(i)
        if (i > 2){
            return@forEach
        }
    }
    for (i in 1..10) {
        if (i > 4) {
            println("停止")
            return
        }
    }
    println("a")
}