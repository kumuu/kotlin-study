@file:Suppress("DEPRECATED_IDENTITY_EQUALS")

/**
 * @Package
 * @auhter 枯木Kum
 * @date 2021/6/4-9:02 AM
 * <p>...</p>
 * @version V1.0
 */

fun main(args: Array<String>) {
    f(1, 2)
    println(f1(1, 2))
    println(f2())
    for (i in 1..10 step 2) {
        println(i)
    }

    var i = 100_000f
    var i1 = 100_000f
    i = 100_000f
    println(i === i1)
    println(i == i1)
    val s = """
        asasas
        asdad
        qwowowwad
    """.trimIndent()

    val arr = arrayOf(2, 2, "8", s)

    mainLoop@ for ((index, item) in arr.withIndex()) {
        println("索引${index}的内容是\n $item")
        for (i in 1..10) {
            if (i > 5) {
                println("使用标记停止")
                break@mainLoop
            }
        }
    }

    when (arr[0]) {
        in 1..2 -> println("111")
        3, 4 -> println("1")
        else -> println("2")
    }
    count(1,2,3,4,5,6,7,8,3,423,42,2)
}


fun f(a: Int, b: Int) = println(a + b)

fun f1(a: Int, b: Int): String {
    return (a + b).toString()
}

fun f2(): String? {
    val s: String? = "ssssssssadass"
    return "${s ?: "ssss".replace("s", "v")},HHHH"
}

fun count(vararg i:Int){
    var result = 0
    for (ii in i) {
        result += ii
    }
    println(result)
}
