/**
 * @Package
 * @auhter 枯木Kum
 * @date 2021/6/4-8:15 PM
 * <p>...</p>
 * @version V1.0
 */

fun main() {
    用户信息 寻找年龄大于 1
}

infix fun List<用户>.寻找年龄大于(age: Int) {
    filter {
        it.age > age
    }.forEach {
        val (name, age) = it
        println("name:$name,age:$age")
    }
}

data class 用户(var name: String, var age: Int)

var 用户信息 = listOf(
    用户("张三", 18),
    用户("李四", 28),
    用户("王二", 10)
)